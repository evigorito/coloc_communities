

library(data.table)
library("RColorBrewer")
library(mygene)
library("clusterProfiler")
library(org.Hs.eg.db)
## library(qusage)

source("/home/ev250/communities/Functions/aux_leiden.R")

## This version deals with activating conda from shell command

## inputs

## chrom.dist <- "/home/ev250/rds/rds-mrc-bsu/ev250/communities/objects/distance/MH_MB.rds"
## comm_meta <- "/home/ev250/rds/rds-cew54-wallace-share/People/Chris/Elena/clustering_for_elena_230523.csv"
## out.mat <- "/home/ev250/rds/rds-mrc-bsu/ev250/communities/objects/layers_mat/adj_mat_each_layer.rds"
## paths <- c("/home/ev250/rds/rds-cew54-wallace-share/People/Chris/Elena/pathways/h.all.v2022.1.Hs.symbols.gmt", "/home/ev250/rds/rds-cew54-wallace-share/People/Chris/Elena/pathways/c2.cp.kegg.v2022.1.Hs.symbols.gmt", "/home/ev250/rds/rds-cew54-wallace-share/People/Chris/Elena/pathways/c2.cp.reactome.v2022.1.Hs.symbols.gmt")
## n <- c("h", "kegg", "reactome")
## path.mat <- "/home/ev250/rds/rds-mrc-bsu/ev250/communities/objects/layers_mat/jac4pathways.rds"
## ave.out <- "/home/ev250/rds/rds-mrc-bsu/ev250/communities/objects/ave_mats"

## args <- commandArgs(trailingOnly = TRUE)

## print(args)

## chrom.dist <- args[1]
## comm_meta <- args[2]
## paths <- args[3]
## n <- args[4]

## out.mat <- args[5]
## path.mat <- args[6]
## tcb.mat <- args[7]
## ave.out <- args[8]
## genes.out <- args[9]


chrom.dist <- snakemake@input[['chrom']]
comm_meta <- snakemake@input[['communities']]
paths <- snakemake@input[['paths']]
n <- snakemake@params[['n']]

out.mat <- snakemake@output[['out']]
path.mat <- snakemake@output[['path_mat']]
tcb.mat <- snakemake@output[['tcb']]
ave.out <- snakemake@params[['ave_dir']]
genes.out <- snakemake@output[['genes']]

##' Combine adj matrices for clustering and make adj matrix connecting genes through pathways
##'
##' @param paths file with annotation pathways for "h", "kegg" and "reactome"
##' @param n names for paths files (defaults to h, kegg, reactome)
##' @param chrom.dist object with distance matrices based on chrom states
##' @param comm_meta file with communities metadata
##' @param out.mat adj matrix for each layer 
##' @param path.mat file name to save adj matrices for linking genes using pathways
##' @param tcb.mat file name to save adj matrices for traits, celltypes and biomarkers
##' @param ave.out dir to output adj matrices
##' @param genes.out name for file to save genes in communities with entrezid for pathway analysis
##' @return saves output 
##' cluster.prep()

cluster.prep <- function(paths, n=c("h", "kegg", "reactome"), chrom.dist, comm_meta,  out.mat, path.mat, tcb.mat, ave.out, genes.out){

    ## GET community meta data
    cl <- fread(comm_meta)
    cl[, cluster.id := paste(block,cluster,sep="_") ]

    ## cluster file has duplicated rows, remove duplicates
    cl <- unique(cl)

    ## correcting celltypes
    cl[celltype == "" & class == "expr", celltype := gsub(".*_([a-z]+).EN.*", "\\1", trait)]
    cl[celltype == "naive" & class == "expr",celltype := gsub(".*_([a-z]+_naive).EN.*", "\\1", trait)]

    ## Select communities with at least one 'trait' (disease), we are only interested in communities around traits

    cl[,ntraits:=sum(class=="trait"),by="cluster.id"]

    cl <- cl[ntraits > 0]


    ## Look at genes and pathways ##

    ens <- cl[class=="expr" & symbol == "", .N, .(ensg)]

    sym <- getGenes(ens$ensg, fields='symbol')
    sym <-  as.data.table(sym)[, .(query, symbol)][!is.na(symbol),]

    setnames(sym, "symbol", "sym")

    genes <- cl[class=="expr",]

    genes <- merge(genes, sym, by.x="ensg", by.y="query", all.x=T)

    genes[!is.na(sym) & symbol == "", symbol := sym]

    genes <- genes[symbol != "",]

    ## Get entrezid
    entrez <- as.data.table(bitr(gene=unique(genes$symbol), fromType='SYMBOL', toType='ENTREZID', OrgDb="org.Hs.eg.db"))

    genes <- merge(genes, entrez, by.x="symbol", by.y="SYMBOL")

    write.table(genes, genes.out, row.names=F)
    

    ## Make traits, biomarkers and celltype adj matrices (based on Chris code https://github.com/chr1swallace/coloc-communities/blob/main/create_jacs.R)

    ns <- setNames(c("shortname","celltype", "shortname"), c("biomarker", "expr", "trait")) 
    
    adj.mats <- mapply(function(a,b) jacmat(cl, a, b),
                       a=names(ns),
                       b=ns,
                       SIMPLIFY=F)
    
    names(adj.mats) <- paste("jac", c(names(ns)[1], ns[2], names(ns)[3]), sep="_")

    saveRDS(adj.mats, tcb.mat)

    ## Create pathways mat

    ens <- cl[class=="expr" & symbol == "", .N, .(ensg)]

    sym <- getGenes(ens$ensg, fields='symbol')
    sym <-  as.data.table(sym)[, .(query, symbol)][!is.na(symbol),]

    setnames(sym, "symbol", "sym")

    genes <- cl[class=="expr",]

    genes <- merge(genes, sym, by.x="ensg", by.y="query", all.x=T)

    genes[!is.na(sym) & symbol == "", symbol := sym]

    genes <- genes[symbol != "",]

    ## Get entrezid
    entrez <- as.data.table(bitr(gene=unique(genes$symbol), fromType='SYMBOL', toType='ENTREZID', OrgDb="org.Hs.eg.db"))

    genes <- merge(genes, entrez, by.x="symbol", by.y="SYMBOL")


    ## Get pathways
    pth <- lapply(paths, qusage::read.gmt)

    ## Build up an adjacency matrix connecting genes using pathways
    
    adj <- lapply(pth, function(i) path2mat(genes, i))
    names(adj) <- names(mapply(function(a,b) grep(a, b),
                         a=n,
                         b=basename(paths)))


    saveRDS(adj,path.mat)

    chrom <- readRDS(chrom.dist)
    ## make row/col names of chrom compatible with other matrices

    n <- sub("cluster", "", colnames(chrom[[1]]))
    chrom <- lapply(chrom, function(i) {
        mat <- copy(i)
        colnames(mat) <- rownames(mat) <- n
        return(mat)
    })

    ## select the matrices to use, make them all adj.matrices 
    chrom.ad <- lapply(chrom, function(i) 1-i)

    mat.l <- c(adj.mats, adj , chrom.ad)

    ## Prepare consensus adj matrix for clustering all layers together    
    all.rows <- Reduce(union, lapply(mat.l, rownames))

    ## add the missing rows/cols to each matrix

    m.complete <- lapply(mat.l, function(m) {
        w <- which(! all.rows %in% rownames(m))
        ## create matrices to bind to m
        mr <- matrix(NA, nrow=length(w), ncol=ncol(m), dimnames=list(all.rows[w], colnames(m)))
        mc <- matrix(NA, nrow=length(w) + nrow(m), ncol=length(w), dimnames=list(c(rownames(m), all.rows[w]), all.rows[w]))
        m <- rbind(m, mr)
        m <- cbind(m, mc)
        m <- m[all.rows, all.rows]
        diag(m) <- 1
        return(m)
    }
    )

    ## Save m.complete

    saveRDS(m.complete, out.mat)


    ###############################################
    ## Making average matrix combining all layers  ##
    ###############################################

    ## Select reactome for pathway matrix and each of the chromatin matrices

    m.reac <- m.complete[which(!names(m.complete) %in% c("kegg", "h"))]
    m.bh <- m.reac[which(!names(m.reac) %in% "MH")]
    m.hl <- m.reac[which(!names(m.reac) %in% "MB")]

    l <- list(bh=m.bh, hl=m.hl)

    l2 <- lapply(l, wrap_meanByRow)   

    ## Save matrices    

    saveRDS(l2, paste0(ave.out, "/traits_reactome_chrom.rds"))

    
    ## ###########################################################
    ## Making average matrix leaving one out except IMD layer  ##
    #############################################################

    common <- Reduce(intersect, lapply(l ,names))
    ex <- which(common != 'jac_trait')

    ## make average leave one out when the chromatin layer is one of them
    mats.1out.by.chromatine <- lapply(names(l), function(i) {       
        
        ## remove each matrix in ex and calculate average
        each.out <- lapply(ex, function(x) {
            mat1 <-  wrap_meanByRow(l[[i]][-x])
        }
        )
        names(each.out) <- names(l[[i]])[ex]

        saveRDS(each.out, paste0(ave.out, "/", i, "_each.excluded.rds"))

    })

    ## make average excluding chromatine layer

    ex.chrom <- wrap_meanByRow(m.reac[names(m.reac) %in% common])

    saveRDS(ex.chrom, paste0(ave.out, "/excluded.chromatin.layer.rds"))
    
   

}

cluster.prep(paths, n, chrom.dist, comm_meta,  out.mat, path.mat, tcb.mat, ave.out, genes.out)
