###################################################################################
## Snakefile for clustering coloc signals V2
# Here I use some of the files generated in V1 and update analysis as
# the input files have been also updated
###################################################################################

shell.prefix("source ~/.bashrc; ")

configfile: "../config.yaml"

localrules: all

import numpy
import pandas as pd
import os

from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
FTP = FTPRemoteProvider()
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider()

# Functions

def select_samples_chromstates(File, sep="\t", search_values = ['HSC & B-cell', 'Blood & T-cell'], exclude=['BSS00313']):
    """Selects values in column GROUP and creates a dictionary with keys unique values for infoline column and values id column. The default File is the metadata file. """
    
    data=pd.read_csv(File, sep=sep)
    data=data[data.GROUP.str.contains('|'.join(search_values))]
    dic={}

    for k in data.infoline.unique():
        
        dic[k]=[ x for x in data.loc[data['infoline'] == k, 'id'].tolist() if x not in exclude]
    return(dic)

def credset_id(f):
    """Given a file with rows credset id, make a list"""
    data=pd.read_csv(f, sep=" ")
    return(data['id'].tolist())
    

# wildcards
paths=["h.all.v2023.1.Hs.symbols.gmt", "c2.cp.kegg.v2023.1.Hs.symbols.gmt", "c2.cp.reactome.v2023.1.Hs.symbols.gmt", "c2.cp.biocarta.v2023.1.Hs.symbols.gmt", "c2.cp.pid.v2023.1.Hs.symbols.gmt","c2.cp.wikipathways.v2023.1.Hs.symbols.gmt","c7.immunesigdb.v2023.1.Hs.symbols.gmt"]

dic_18states = select_samples_chromstates(File=config['communities'] + "/chrom18states/main_metadata_table.tsv")

chrom_samples_id = [item for l in list(dic_18states.values()) for item in l]

N=credset_id(config['out_dir'] + "/objects/credsets2use_230711/credset.id.txt")

mc_algs=["maxpear"]

algs=mc_algs

dates=["230711"]

rule all:
    input:
        # expand(config['out_dir'] + "/objects/pathways/{path}", path=paths)
        # expand(config['out_dir'] + "/objects/credsets_{date}/credset_trimmed_{n}.rds", n=N, date=dates),
        # expand(config['out_dir'] + "/objects/credsets_chromstates_{date}/credset_trimmed_chromstates_{n}.txt", n=N, date=dates)
        # expand(config['out_dir'] + "/objects/credsets2use_{date}/credset.id.txt", date=dates)
        # expand(config['communities'] + "/credsets_chromstates_{date}.rds", date=dates)
        # expand(config['out_dir'] + "/objects/distance_{date}/MH_MB.rds", date=dates)       
        # expand(config['out_dir'] + "/objects/layers_mat_{date}/bio_traits_celltype_adjmatx.rds", date=dates),
        # expand(config['out_dir'] + '/objects/ave_mats_{date}/traits_reactome_chrom.rds', date=dates)
        # config['out_dir'] + "/objects/layers_mat/genes.in.communities.entrezid.txt"
        # expand(config['out_dir'] + '/objects/{mcclust}_{date}/blocks_per_layer.rds', date=dates, mcclust=mc_algs),
        # expand(config['communities'] + "/maxpear/chrom_imd_{date}.rds", date=dates),
        # expand(config['out_dir'] + "/objects/tf_sites_{date}/tf_prediction_credset_{n}.txt",date=dates, n=N),
        # expand(config['out_dir'] + "/objects/tf_mats_{date}/adj_mat_score.txt", date=dates),
        # expand(config['out_dir'] + "/objects/tf_mats_{date}/adj_mat_pp.txt", date=dates)
        # expand(config['out_dir'] + "/objects/layers_mat_{date}/tfs.entrezid.txt", date=dates)
        # expand(config['out_dir'] + "/objects/layers_mat_{date}/tfs4pathways.rds", date=dates)
        # expand(config['out_dir'] + "/objects/tf_mats_{date}/adj_mat_tfs2pathways.rds", date=dates),
        "Scripts/tf_report.html"

rule down_pathways_2023:
    """Download annotated pathways from GSEA, updated version 2023 (communities has 2022"""
    params:
        url="https://data.broadinstitute.org/gsea-msigdb/msigdb/release/2023.1.Hs/{path}",
        out_dir=config['out_dir'] + "/objects/pathways"
    output:
        config['out_dir'] + "/objects/pathways/{path}"
    shell:
        "cd {params.out_dir} ;"
        "wget -O {wildcards.path} {params.url}" 


rule credsetQC:
    """From the r object provided by Chris with the credible sets for GWAS signals select the credible sets for disease, remove SNPs if their PP is 10-3 lower than that max PP. Also, exclude trait "ASTE_Ferreira_29083406_1-hg38.tsv.gz" (not a disease, just a comparison of asthma with eczema, and blocks "chr6_block25|chr6_block26|chr6_block27" are MHC, too messy. Save a file with each row the name of the credible set. This file will be use to create the wildcard for the output files in the following rules."""
    input:
        snps=config['communities'] + "/clustering_for_elena_{date}.csv",
        credsets=config['communities'] + "/credsets_for_elena_{date}.RData",
    output:
        out=config['out_dir'] + "/objects/credsets2use_{date}/credset.id.txt"
    script:
        "Scripts/credsetQC.R"
        
rule annotation_chromstates1:
    """From the r object provided by Chris with the credible sets for GWAS signals select the credible sets for disease, remove null elements, remove SNPs within credible sets names as 'null' and save each credible set as named list with name the block. Each list has a named vector for the SNPs in the credible set, with values the PP."""
    input:
        snps=config['out_dir'] + "/objects/credsets2use_{date}/credset.id.txt",
        credsets=config['communities'] + "/credsets_for_elena_{date}.RData",
    output:
        out=expand(config['out_dir'] + "/objects/credsets_{{date}}/credset_trimmed_{n}.rds", n=N)
    script:
        "Scripts/chromstates.R"

rule annotation_chromstates2:
    """ Using the output files from annotation_chromstates1 get overlaps with chromstates."""
    input:
        credset=config['out_dir'] + "/objects/credsets_{date}/credset_trimmed_{n}.rds",
        chromstates=expand(config['out_dir'] + "/chrom18states/{id}_18_CALLS_segments.bed.gz", id=chrom_samples_id),
        snps=config['communities'] + "/clustering_for_elena_{date}.csv",
    resources:
        tmpdir=config['out_dir']
    threads:
        4
    output:
        out=config['out_dir'] + "/objects/credsets_chromstates_{date}/credset_trimmed_chromstates_{n}.txt"
    script:
        "../Scripts/chromstates2.R"

rule credset_chromstate:
    """Compute probabilities of active, inactive and quiescent chromatine state integrating info across credible set"""
    input:
        chromstates=expand(config['out_dir'] + "/chrom18states/{id}_18_CALLS_segments.bed.gz", id=chrom_samples_id),
        snps=config['communities'] + "/clustering_for_elena_{date}.csv",
        credsets=expand(config['out_dir'] + "/objects/credsets_chromstates_{{date}}/credset_trimmed_chromstates_{n}.txt", n=N),
    output:
        out=config['communities'] + "/credsets_chromstates_{date}.rds"
    script:
        "../Scripts/credset_chromstate.R"

rule distance_SNPs:
    """Create a matrix of distance across SNPs by chromstates. Code adapted from Chris. Params "p" corresponds to the probablility threshold for quiescence. Any SNP-pairs with all samples above this threshold will get NA. Saves a list with each element a matrix with Hellinger (MH) or Bhattacharyya distance (MB) """
    input:
        data=config['communities'] + "/credsets_chromstates_{date}.rds"
    params:
        p=0.9
    output:
        out=config['out_dir'] + "/objects/distance_{date}/MH_MB.rds"
    script:
        "../Scripts/distances.R"


rule prepare_cluster1:
    """Prepare data for clustering. Due to libraries incompatibilities I need to split the original code from rule prepare_cluster in communities/Snakefile into 3 parts. Comms2use has the names of the communities to include in the analysis, removing MHC and "ASTE_Ferreira_29083406_1-hg38.tsv.gz" (not a disease, just a comparison of asthma with eczema) """
    input:
        chrom=config['out_dir'] + "/objects/distance_{date}/MH_MB.rds",
        communities=config['communities'] + "/clustering_for_elena_{date}.csv",
        comms2use=config['out_dir'] + "/objects/credsets2use_{date}/credset.id.txt"
    output:      
        tcb=config['out_dir'] + "/objects/layers_mat_{date}/bio_traits_celltype_adjmatx.rds",
        genes=config['out_dir'] + "/objects/layers_mat_{date}/genes.in.communities.entrezid.txt",
    script:
        "Scripts/prepare_cluster1.R"
        
        
rule path_mats:
    """I can only use qusage in rleiden, but having trouble installing clusterprofiler in rleiden. I split the rule prepare_cluster in communities to run each piece of code in the compatible R"""
    input:
        paths=expand(config['communities'] + "/pathways/{path}", path=paths[0:3]),
        genes=config['out_dir'] + "/objects/layers_mat_{date}/genes.in.communities.entrezid.txt",
        r=config['rscript'],
        script="Scripts/path_mats.R"
    params:
        n=["h", "kegg", "reactome"],
    output:
        path_mat=config['out_dir'] + "/objects/layers_mat_{date}/jac4pathways.rds"
    shell:
        "conda activate rleiden ;"
        "{input.r}  {input.script} {input.genes} {input.paths} {params.n} {output.path_mat} "

rule prepare_cluster2:
    """Prepare data for clustering, make adj matrices (re-do traits, biomarkers and celltype as communities file has changed) and do basic QC. Use conda rleiden to use package qusage. Saves all adj matrices into out file, with same cols and rows(out). Saves pathway matrices (path_mat)  and traits, bio, cell types for the communities that have information (tbc). Saves average matrices using reactome for pathways and each chromatine layer (ave_mat dir). Saves average leave one out using reactome and each chromative layer (ave_mat_dir)"""
    input:
        chrom=config['out_dir'] + "/objects/distance_{date}/MH_MB.rds",
        tcb=config['out_dir'] + "/objects/layers_mat_{date}/bio_traits_celltype_adjmatx.rds",
        path_mat=config['out_dir'] + "/objects/layers_mat_{date}/jac4pathways.rds"
    params:
        ave_dir=config['out_dir'] + "/objects/ave_mats_{date}"
    output:
        out=config['out_dir'] + "/objects/layers_mat_{date}/adj_mat_each_layer.rds",
        out2=config['out_dir'] + "/objects/ave_mats_{date}/excluded.chromatin.layer.rds"
    script:
        "Scripts/prep_cluster2.R"

rule mccluster_ave:
    """Cluster using mcclust and do QC. For QC, take the all the adj matrices and leave each layer out except for diseases traits. Make consensus matrix and then cluster testing different algorithms. Compare algorithms using the layer that was left out"""
    input:
        communities=config['communities'] + "/clustering_for_elena_{date}.csv",
        adj_mats=config['out_dir'] + "/objects/layers_mat_{date}/adj_mat_each_layer.rds",
    params:
        ave_dir= config['out_dir'] + "/objects/ave_mats_{date}",
        out_ob=config['out_dir'] + "/objects/{mcclust}_{date}",
        n=["h", "kegg", "reactome"],
    output:
        ob=config['out_dir'] + '/objects/{mcclust}_{date}/blocks_per_layer.rds'
    script:
        "Scripts/cluster_ave_mcclust.R"

rule cluster_maxpear:
    """Based on the analysis so far maxpear seems to be giving a reasonable number of clusters, prepare files for Chris"""
    input:
        adj_mats=config['out_dir'] + "/objects/layers_mat_{date}/adj_mat_each_layer.rds",
        communities=config['communities'] + "/clustering_for_elena_{date}.csv",
        r=config['rscript'],
        script="Scripts/maxpear4Chris.R"
    params:
        out_dir=config['communities'] + "/maxpear"
    output:
        out=config['communities'] + "/maxpear/chrom_imd_{date}.rds"
    shell:
        "conda activate rleiden;"
        "{input.r}  {input.script} {input.adj_mats} {input.communities} "
        "{params.out_dir} {wildcards.date} "    
               

rule tf_annot:
    """Annotate TF binding/disruption for the SNPs in cred sets using motifbreakR library (used by Helen)"""
    input:
        credset=config['out_dir'] + "/objects/credsets_{date}/credset_trimmed_{n}.rds",
        r=config['r43'],
        script="Scripts/tf.R"
    resources:
        tmpdir=config['out_dir']
    threads:
        4
    params:
        refdir=config['ref38'],
        # pattern="ALL.chr*.shapeit2_integrated_v1a.GRCh38.20181129.phased.vcf.gz"
    output:
        out=config['out_dir'] + "/objects/tf_sites_{date}/tf_prediction_credset_{n}.txt",
    shell:
        "conda activate r43;"
        "{input.r}  {input.script} {input.credset} {params.refdir} "
        "{output.out} " 

rule tf_adj:        
    """ Prepare adj matrix linking communities based on TF binding sites. Input the prediction for biding site disruption obtained from rule tf_annot. Output mat_score uses column "alleleEffectSize" to weight the posterior probability for each SNP contributing to a given TF with a 'strong' effect (motifbreak output), while mat_pp ads the credible set pp for those SNPs contributing to a TF with strong effect"""
    input:
        tf=expand(config['out_dir'] + "/objects/tf_sites_{{date}}/tf_prediction_credset_{n}.txt",n=N)
    output:
        mat_score=config['out_dir'] + "/objects/tf_mats_{date}/adj_mat_score.txt",
        mat_pp=config['out_dir'] + "/objects/tf_mats_{date}/adj_mat_pp.txt"
    script:
        "Scripts/tf_adj.R"

rule tf_symbol:
    """Prepare adj matrix linking communities based on shared pathwyas across predicted TF binding sites, selecting TFs with "strong" effect (motifbreak output).First step is to get gene entrez id for the TFs."""
    input:
        tf=expand(config['out_dir'] + "/objects/tf_sites_{{date}}/tf_prediction_credset_{n}.txt",n=N)
    output:
        genes=config['out_dir'] + "/objects/layers_mat_{date}/tfs.entrezid.txt"
    script:
        "Scripts/entrez.tf.R"
        
rule tf_pathways:
    """Link TFs predicted to have binding sites disrupted with strong effect for community SNPs via pathways"""
    input:
        tf_ids=config['out_dir'] + "/objects/layers_mat_{date}/tfs.entrezid.txt",
        tf=expand(config['out_dir'] + "/objects/tf_sites_{{date}}/tf_prediction_credset_{n}.txt",n=N),
        paths=expand(config['communities'] + "/pathways/{path}", path=paths[0:3]),
        r=config['rscript'],
        script="Scripts/path_tfs.R"
    params:
        n=["h", "kegg", "reactome"],
    output:
        path_mats=config['out_dir'] + "/objects/tf_mats_{date}/adj_mat_tfs2pathways.rds"
    shell:
        "conda activate rleiden ;"
        "{input.r}  {input.script} {input.tf_ids} {input.tf} {input.paths} {params.n} {output.path_mats} "
    

rule mat_cluster2:
    """Try maxpear cluster on the average matrix between the most connected adjacency matrices"""
    input:
        mats=config['out_dir'] + "/objects/layers_mat_{date}/adj_mat_each_layer.rds",
        tf_path=config['out_dir'] + "/objects/tf_mats_{date}/adj_mat_tfs2pathways.rds"
    output:
        mat_ave=""
    script:
        "Scripts/cluster2.R"


rule chris_code:
    """Look at Chris code /home/ev250/rds/rds-mrc-bsu/ev250/communities/chris_code"""
    input:
        ""
    output:
        ""
    script:
        "/home/ev250/rds/rds-mrc-bsu/ev250/communities/chris_code/four-matrix-clustering.R"

rule plot_clusters:
    """After running the code in chris_code rule I saved objects imd_cl1.4.d.txt and bio_cl1.4.d.txt. Using those files make plots to visualise imd and biomarkers enrichment in each cluster when using each cluster strategy"""
    input:
        imd=config['out_dir'] + "/objects/maxpear_230711/imd_cl1.4.d.txt",
        bio=config['out_dir'] + "/objects/maxpear_230711/bio_cl1.4.d.txt",
    output:
        "Scripts/imd_bio.html"
    script:
        "Scripts/imd_bio.R"

rule notes:
    """MAke notes for cluster analysis from chris_code rule"""
    input:
        "Scripts/cluster_notes.R"


    
        
# snakemake --use-conda
# snakemake  -k -j 500 --cluster-config ../cpu.json --cluster "sbatch -A {cluster.account} -p {cluster.partition}  -c {cluster.cpus-per-task}   -t {cluster.time} --output {cluster.error} -J {cluster.job} "

# snakemake --cleanup-metadata <outfile> (with bash wildcards for many files): to avoid re-running after changing formatting, etc of a script file.

